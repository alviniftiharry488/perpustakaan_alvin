import 'package:flutter/material.dart';
import 'package:perpustakaan_alpin/home.dart';

class InputDatapage extends StatefulWidget {
  InputDatapage({Key key}) : super(key: key);

  @override
  _InputDatapageState createState() => _InputDatapageState();
}

class _InputDatapageState extends State<InputDatapage> {
  final appBar = AppBar(
    elevation: .5,
    // leading: IconButton(
    //   icon: Icon(Icons.menu),
    //   onPressed: () {},
    // ),
    title: Text('Perpustakaan Online'),
    actions: <Widget>[
      IconButton(
        icon: Icon(Icons.search),
        onPressed: () {},
      )
    ],
  );

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: appBar,
        body: Container(
          margin: EdgeInsets.all(20),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: TextField(
                    // Tell your textfielit owns

                    // Every single time the text changes in a
                    // TextField, this    // and it passes in the value.
                    //
                    // Set the text to
                    // to the next value.

                    decoration: InputDecoration(
                  labelText: 'Nama Buku',
                )),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: TextField(
                    decoration: InputDecoration(
                  labelText: "Penulis Buku",
                )),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: TextField(
                    decoration: InputDecoration(
                  labelText: 'Sinopsis Buku',
                )),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: TextField(
                    decoration: InputDecoration(
                  labelText: 'Gambar Url Buku',
                )),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Builder(
                  builder: (context) {
                    return RaisedButton(
                      onPressed: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) => Home()));
                      },
                      color: Colors.indigoAccent,
                      child: Text('Tambah Buku'),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
